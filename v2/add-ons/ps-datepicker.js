var dp = angular.module('psDatepicker', ['mgcrea.ngStrap.datepicker']);

dp.directive('psDatepicker', function() {
  return {
    restrict: 'E',
    scope: {
      ngModel:    '=',
      id:         '@',
      ngRequired: '&'
    },
    templateUrl: 'add-ons/ps-datepicker.html',
    link: function(scope, element, attrs) {
      if (! attrs.ngModel) {
        throw new Error("<ps-datepicker>: ng-model attribute is required.");
      }

      if (! attrs.ngRequired) {
        scope.ngRequired = function() { return true; }
      }

      scope.opened = false;
      scope.open = function(event) {
        event.preventDefault();
        event.stopPropagation();
        console.log('open');
        scope.opened = true;
      };

      scope.minDate = new Date(1900, 0, 1);
      scope.maxDate = new Date();

    }
  }
});
