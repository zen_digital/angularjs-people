/*
   Angular utilities (filters, directives, etc.)
 */
var util = angular.module('util', []);



util.directive('messageBox', function() {
return {
  restrict: 'E',
  scope: {
    ngModel: '=',
    type:    '@',
    icon:    '@'
  },
  transclude: true,
  templateUrl: 'templates/message-box.html',
  replace: false,

  link: function(scope, element, attrs) {
    if (! attrs.ngModel)
      throw new Error('<message-box>: missing required ng-model attribute');
    if (! attrs.type)
      throw new Error('<message-box>: missing required type attribute');
    if (! attrs.icon)
      throw new Error('<message-box>: missing required icon attribute');

    scope.close = function() {
      scope.ngModel = null;
    }
  }
}
});


util.filter('gender', function() {
  return function(input) {
    switch(input) {
      case "M": return "male";
      case "F": return "female";
      default:  return "unknown";
    }
  }
});

util.factory('psNotify', function($rootScope, $timeout) {
  $rootScope.warning = null;
  $rootScope.error   = null;

  return {
    showWarning: function(msg) {
      if (msg) {
        $rootScope.warning = msg;
        $timeout(function() { $rootScope.warning = null }, 3000);
      }
    },
    showError: function(msg) {
      if (msg) {
        $rootScope.error = msg;
        $timeout(function() { $rootScope.error = null }, 3000);
      }
    }
  }
});

util.factory('psHttp', function($http, psNotify, $q) {
  function handleError(errorData) {
    psNotify.showError(errorData.error);
  }

  function handleHttp(httpPromise) {
    var deferred = $q.defer();

    httpPromise.
    success(function(data) {
      deferred.resolve(data);
    }).
    error(function(errData) {
      handleError(errData);
      deferred.reject(errData);
    });

    return deferred.promise;
  }
    
  return {
    get: function(url) {
      return handleHttp($http.get(url));
    },
    post: function(url, data) {
      return handleHttp($http.post(url, data));
    }
  }
});

