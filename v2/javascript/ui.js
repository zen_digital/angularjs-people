var app = angular.module('peopleApp', ['util', 'psDatepicker', 'ngRoute', 'ngCookies']);

app.config(function($routeProvider) {
  $routeProvider.
    when('/search', {
      templateUrl: 'search.html',
      controller:  'PeopleSearchCtrl'
    }).
    when('/edit/:id', {
      templateUrl: 'edit.html',
      controller:  'EditCtrl'
    }).
    when('/show/:id', {
      templateUrl: 'show.html',
      controller:  'ShowCtrl'
    }).
    otherwise({
      redirectTo: '/search'
    });
});

app.controller('PeopleSearchCtrl', function ($scope, psHttp, $rootScope, $cookies, $location) {
  $scope.searchPrefix = "";
  $scope.people       = [];
  $scope.predicate    = 'last';
  $scope.reverse      = false;

  $scope.checkSearch = function() {
    var value = $scope.searchPrefix ? $scope.searchPrefix : "";
    if (value.length < 2) {
      $scope.people = [];
    }
    else {
      var url = server + "/people/" + $scope.searchPrefix;

      $cookies.put('previousSearch', $scope.searchPrefix);
      $location.search('q', $scope.searchPrefix);
      // Setting the query string seems to cause the search input
      // to lose mouse focus. This kludge puts it back.
      $("#search-input").focus();

      psHttp.get(url).then(function(data) {
        $scope.people = data.people;
      });
    }
  };

  $scope.toggleSortOrder = function(column) {
    if (column === $scope.predicate) {
      // User clicked on the same column that's already being used to sort.
      // Reverse the sort.
      $scope.reverse = !$scope.reverse;
    }
    else {
      // User clicked on a different column. Sort in ascending order.
      $scope.predicate = column;
      $scope.reverse = false;
    }
  };

  var previousSearch = $location.search().q;
  if (! previousSearch) {
    previousSearch = $cookies.get('previousSearch');
  }

  if (previousSearch) {
    $scope.searchPrefix = previousSearch;
    $scope.checkSearch();
  }
});

app.controller('EditCtrl', function ($scope, psHttp, $routeParams, $location) {
  $scope.genders = { 'F': 'female', 'M': 'male' };

  $scope.editingPerson = null;

  psHttp.get(server + "/person/" + $routeParams.id).then(function(data) {
    $scope.editingPerson = data.person;
  });

  function returnToMainPage() {
    $location.path("/");
  }

  $scope.cancelEdit = function() {
    returnToMainPage();
  };

  $scope.save = function(isValid) {
    if (isValid) {
      var url = server + "/person/" + $scope.editingPerson.id;

      psHttp.post(url, $scope.editingPerson).then(function(data) {
        returnToMainPage();
      });
    }
    else {
      notify.showWarning("Invalid form data.");
    }
  }
});

app.controller('ShowCtrl', function($scope, $routeParams, psHttp) {
  $scope.person = null;

  psHttp.get(server + "/person/" + $routeParams.id).then(function (data) {
    $scope.person = data.person;
  })
});

app.controller('AlertCtrl', function ($scope, $rootScope) {
  $rootScope.modalAlert = null;

  $rootScope.$watch("modalAlert", function(newValue) {
    if (newValue !== null)
      $("#alert-modal").modal('show');
    else
      $("#alert-modal").modal('hide');
  });

  $scope.closeModal = function() {
    $rootScope.modalAlert = null;
  }
});
