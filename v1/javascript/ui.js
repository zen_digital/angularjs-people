var app = angular.module('peopleApp', ['util']);

// start-excerpt controller
app.controller('PeopleSearchCtrl', function ($scope, $http) {
// end-excerpt controller

  $scope.searchPrefix = "";
  $scope.people       = [];
  $scope.predicate    = 'last';
  $scope.reverse      = false;

  $scope.checkSearch = function() {
    var value = $scope.searchPrefix ? $scope.searchPrefix : "";
    if (value.length < 2) {
      $scope.people = [];
    }
    else {
      search(value);
    }
  };

  $scope.toggleSortOrder = function(column) {
    if (column === $scope.predicate) {
      // User clicked on the same column that's already being used to sort.
      // Reverse the sort.
      $scope.reverse = !$scope.reverse;
    }
    else {
      // User clicked on a different column. Sort in ascending order.
      $scope.predicate = column;
      $scope.reverse = false;
    }
  };

  $scope.hideWarning = function() {
    $scope.warning = null;
  };

  // start-excerpt search
  var search = function (prefix) {
    var url = server + "/people/" + prefix;

    $http.get(url).success(function (data) {
      // end-excerpt search
      $scope.people = data.people;
    });
  }
});


